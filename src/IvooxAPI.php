<?php

namespace SemanticJail\IvooxAPI;

class IvooxAPI
{

    const IVOOX_URL = 'http://www.ivoox.com';

    public function searchPodCast(string $query, int $page = null)
    {

        $result = [];


        $index = is_null($page) ? 1 : $page;


        while (true) {

            $requestUrl = self::IVOOX_URL . '/' . $query . '_sw_1_' . $index . '.html';
            $html = file_get_contents($requestUrl);
            libxml_use_internal_errors(true);

            $dom = new \DOMDocument();
            $dom->loadHTML($html, LIBXML_NOWARNING);
            $xpath = new \DOMXPath($dom);
            $nodeList = $xpath->query("//div[@class='front modulo-view modulo-type-programa']");

            if ($nodeList->length == 0) break;

            foreach ($nodeList as $node) {

                $url = $node->getELementsByTagName('a')[0]->getAttribute('href');
                if (strpos($url, "_sq_") == 0) continue;
                $imgSrc = $node->getElementsByTagName('img')[0]->getAttribute('src');
                $title = $node->getELementsByTagName('a')[0]->getAttribute('title');
                preg_match('/_sq_(.*?)_1.html/', $url, $id);
                $id = $id[1];

                $result[] = ['id' => $id, 'name' => $title, 'ivoox_url' => $url, 'thumbnail' => $imgSrc];

            }

            if ($index == $page) break;

            $paginator = $xpath->query("//ul[@class='pagination']");
            if (!$paginator) break;

            $links = $paginator->item(0)->getElementsByTagName('a');
            $lastHref = $links[$links->length - 1]->getAttribute('href');

            if ($lastHref == '#') break;
            $index++;

        }


        return $result;

    }

    public function searchEpisodes(string $idPodCast, int $page = null)
    {

        $result = ['name' => '', 'episodes' => []];

        $index = is_null($page) ? 1 : $page;

        while (true) {

            $requestUrl = self::IVOOX_URL . '/test_sq_' . $idPodCast . '_' . $index . '.html';
            $html = file_get_contents($requestUrl);
            libxml_use_internal_errors(true);

            $dom = new \DOMDocument();
            $dom->loadHTML($html, LIBXML_NOWARNING);
            $xpath = new \DOMXPath($dom);

            $podcastName = $dom->getElementById('list_title_new');
            if (!$podcastName instanceOf \DOMElement || strlen(trim($podcastName->textContent)) == 0) break;

            $result['name'] = $podcastName->textContent;

            $nodeList = $xpath->query("//div[@class='front modulo-view modulo-type-episodio']");
            if ($nodeList->length == 0) break;

            foreach ($nodeList as $node) {

                $titleWrapper = $xpath->query(".//p[@class='title-wrapper text-ellipsis-multiple']", $node)->item(0);

                $title = $titleWrapper->getElementsByTagName('a')[0]->getAttribute('title');
                $url = $titleWrapper->getElementsByTagName('a')[0]->getAttribute('href');
                $description = $titleWrapper->getElementsByTagName('button')[0]->getAttribute('data-content');
                $thumbnail = $xpath->query(".//div[@class='header-modulo']", $node)->item(0)->getElementsByTagName('img')[0]->getAttribute('src');
                $duration = $xpath->query(".//p[@class='time']", $node)->item(0)->textContent;
                $likes = trim($xpath->query(".//li[@class='likes']", $node)->item(0)->getElementsByTagName('a')->item(0)->textContent);
                $comments = trim($xpath->query(".//li[@class='comments']", $node)->item(0)->getElementsByTagName('a')->item(0)->textContent);

                $result['episodes'][] = [

                    'name' => $title,
                    'description' => $description,
                    'url' => $url,
                    'duration' => $duration,
                    'thumbnail' => $thumbnail,
                    'likes' => $likes,
                    'comments' => $comments

                ];

            }

            if ($index == $page) break;

            $paginator = $xpath->query("//ul[@class='pagination']");
            if (!$paginator) break;

            $links = $paginator->item(0)->getElementsByTagName('a');
            $lastHref = $links[$links->length - 1]->getAttribute('href');

            if ($lastHref == '#')
                break;

            $index++;

        }

        return $result;

    }

}