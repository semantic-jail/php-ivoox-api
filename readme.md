# Php Ivoox API

Esta clase te permite obtener una respuesta json con programas o episodios de Podcast alojados en la plataforma Ivoox

### Instalación

Incluye en tu composer.json:
```sh
{
    "require": {
        "semantic-jail/ivoox-php-api": "dev-master"
    }
}
```

entonces ejecuta:

```
$ composer update
```

o ejecuta directamente:

```
$ composer require  semantic-jail/ivoox-php-api
```


### Cómo usarlo

Crea una instancia de la clase IvooxAPI

```
$ivoox = new IvooxAPI();
```

#### Métodos

##### Para buscar programas 

```
$ivoox->searchPodcast("programar es una mierda");

Devuelve:

Array
(
    [0] => Array
        (
            [id] => f1432444
            [name] => Programar es una mierda
            [ivoox_url] => http://www.ivoox.com/podcast-programar-es-mierda_sq_f1432444_1.html
            [thumbnail] => http://static-1.ivooxcdn.com/canales/6/2/1/7/6221498657126_MD.jpg
        )

)

```


-También puedes pasar el número de página concreta

```
$ivoox->searchPodcast("programar es una mierda", 1);

```



##### Listado de episodios de un programa


```
$ivoox->searchEpisodes("f1432444");

```


-También puedes pasar el número de página concreta

```
$ivoox->searchEpisodes("f1432444", 2);

Devuelve:

Array
(
    [name] => Programar es una mierda 
    [episodes] => Array
        (
            [0] => Array
                (
                    [name] => Episodio 1 - Scrum [REMASTERED]
                    [description] => En este episodio hablamos de Scrum. Describimos los puntos principales de este framework para la gestión de proyectos. 

Versión [Remastered]

Si quieres oír la versión original puedes descargarla de: http://bit.ly/2mCS4Xt


Música de dilo: www.dilo.org
Entradilla: War Inside.
Final: Sick of reality.
Ambas están bajo licencia http://creativecommons.org/
                    [url] => http://www.ivoox.com/episodio-1-scrum-remastered-audios-mp3_rf_19519341_1.html
                    [duration] => 01:21:57
                    [thumbnail] => http://static-1.ivooxcdn.com/audios/6/0/3/6/8821498656306_SM.jpg
                    [likes] => 22
                    [comments] => 1
                )

        )

)



```

